# Markdown Diagrams <small>(browser extension)</small>

A browser extension for Chrome, Edge, Opera and Firefox that render diagrams and charts code blocks into preview images.

Main repository: https://github.com/marcozaccari/markdown-diagrams-browser-extension
